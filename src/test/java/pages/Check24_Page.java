package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;



public class Check24_Page {

    public Check24_Page() {
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(xpath ="//*[@class='CloseThin-Svg null']")
    public WebElement closeThin;

    @FindBy(xpath = "//a[@class='c24-cookie-consent-button']")
    public WebElement cookies;

    @FindBy(xpath = "//button[@type='submit']")
    public WebElement tarifVergeleichen;

    @FindBy(xpath = "//button[@class='css-1rh89bh c24Submit']")
    public WebElement weiterButton;

    @FindBy(className="css-1sd5il1" )
    public WebElement fotoCHECK;
//Yeni notlar burda mi
    @FindBy(xpath="//span[text()='Fahrzeugschein ']")
    public WebElement fahrzeugscheinButton;

    @FindBy(xpath = "//input[@name='hsn']")
    public WebElement herstelleNummer;

    @FindBy(xpath = "//input[@name='tsn']")
    public  WebElement typSchlüsselNummer;

    @FindBy(xpath = "//input[@name='erstzulassung']")
    public WebElement textBox;

    @FindBy(xpath = "//div[@class='css-j2m412']")
    public WebElement erstzulassungTextMessage;

    @FindBy(xpath = "//input[@name='versicherungsbeginn']")
    public WebElement versicherungBeginDatum;

    @FindBy(xpath = "(//h2[@class='css-11du1ss'])[3]")
    public WebElement nutzungFahrzeugTitle;

}

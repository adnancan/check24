package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

public class Beymen_page {
    public Beymen_page(){
        PageFactory.initElements(Driver.getDriver(),this);
    }
//adjakldfjasklfjksdlfjsdkl unlari eklemegit
    @FindBy(xpath = "(//span[@class='o-header__userInfo--text'])[3]")
    public WebElement sepetim;

    @FindBy(id = "moreContentButton")
    public WebElement dahaFazlaGoster;


    @FindBy(css = "div.m-productImageList")
    public WebElement ilkUrun;

    @FindBy(css = "span.m-variation__item")
    public WebElement bedenSecimi;

    @FindBy(id="addBasket")
    public WebElement sepetEkle;

    @FindBy(id="priceNew")
    public WebElement sayfadakiFiyat;

    @FindBy(className = "m-productPrice__salePrice")
    public WebElement sepetkiFiyat;

    @FindBy(id="quantitySelect0")
    public WebElement adetArtirma;

    @FindBy(css ="span.m-productPrice__salePrice")
    public WebElement toplamFiyat;

    @FindBy(id = "removeCartItemBtn0")
    public WebElement sepetSil;

    @FindBy(xpath = "(//strong[@class='m-empty__messageTitle'])[2]")
    public WebElement silindiEkrani;





}

package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.util.List;

public class Wibu_Test_Page<ckUbersicht> {

    public Wibu_Test_Page() {
        PageFactory.initElements(Driver.getDriver(), this);

    }


    @FindBy(className = "sub-header")
    public WebElement codeMeterText;


    @FindBy(className = "header")
    public WebElement TextStartseite;



    @FindBy(xpath = "(//div[@class='tileimg'])[2]")
    public WebElement containerErstellen;

    @FindBy(xpath = "//*[@class='fas fa-cloud-download-alt mediumtall icon-col']")
    public WebElement iconHerunterladen;


    @FindBy(xpath = "(//button[@class='selection icon'])[1]")
    public WebElement appSettings;

    @FindBy(xpath = "//i[@class='fas fa-question-circle mediumtall icon-col']")
    public WebElement appUserHelp;

    @FindBy(xpath = "(//button[@class='selection icon'])[2]")
    public WebElement appAccount;

    @FindBy(xpath = "//*[text()='Datenschutz']")
    public WebElement datenschutz;

    @FindBy(xpath = "//*[text()='Impressum']")
    public WebElement impressum;

    @FindBy(xpath = "//*[text()='Kontakt']")
    public WebElement kontakt;

    @FindBy(id = "containernav")
    public WebElement containerMenu;


    @FindBy(id = "createcontainersbtn")
    public WebElement containerErstellenButton;

    @FindBy(id = "quantity")
    public WebElement anzahlContainer;

    @FindBy(id = "name")
    public WebElement containerName;

    @FindBy(xpath = "(//button[@type='button'])[2]")
    public WebElement drobdownCmActID;

    @FindBy(xpath = "//div[contains(text(),' 3AAA ')]")
    public WebElement drobdownAuswuhlen;

    @FindBy(id = "nextstepbtn")
    public WebElement weiterButton;


    @FindBy(xpath = "(//*[contains(text(),' Anmeldedaten')])[2]")
    public WebElement anmeldeDaten;

    //5.Test Locator
    @FindBy(xpath ="//div[contains(text(), 'Upload')]")
    public WebElement iconUpload;

    @FindBy(className = "buttonlike")
    public WebElement dateienAuswuhlen;

    //2 Test

    @FindBy(className = "menu-text")
    public List<WebElement> hauptMenuIcons;

    @FindBy(id = "certificatesnav")
    public WebElement zertifikatFarbe;
    @FindBy(id = "downloadsdd")
    public WebElement kleinUploadSymbol;

    @FindBy(className = "dlicon")
    public WebElement schlusselSymbol;

    @FindBy(className = "countcircle")
    public WebElement orangeFarbeSchlusselSymbol;

    @FindBy(id="showcontainersbtn")
    public WebElement containerAnzeigen;

    @FindBy(xpath = "//a[contains(text(),'Container')]")
    public WebElement containerText;

    @FindBy(id = "pathupbtn")
    public WebElement zuruckSymbol;

    @FindBy(id = "homenav")
    public WebElement startseiteIcon;

    @FindBy(id = "importraubtn")
    public WebElement updatedateienEinspielen;

    @FindBy(xpath = "//a[contains(text(),'Update-Dateien')]")
    public WebElement updateDateienText;

    @FindBy(linkText = "Upload")
    public WebElement uploadTitel;

    @FindBy(xpath = "(//button[@class='box-shadow'])[2]")
    public WebElement zuruckUbersicht;

    @FindBy(id = "searchtext")
    public  WebElement suchbegriff;


    @FindBy(xpath = "(//div[@class='nooverflow'])[1]")
    public WebElement gesehenUbersicht;

    @FindBy(className = "error")
    public WebElement errorText;




}

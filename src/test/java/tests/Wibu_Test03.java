package tests;

import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Wibu_Login_Page;
import pages.Wibu_Test_Page;
import utilities.Driver;
import utilities.ReusableMethods;


public class Wibu_Test03 {

    Wibu_Login_Page loginPage = new Wibu_Login_Page();
    Wibu_Test_Page wibuTestPage = new Wibu_Test_Page();

    @Test
    public void HauptMenüTest() {
        loginPage.userLogin();

        String alteFarbeStartSeite=wibuTestPage.zertifikatFarbe.getCssValue("background-color");
        Actions actions=new Actions(Driver.getDriver());
        ReusableMethods.waitFor(3);
        actions.moveToElement(wibuTestPage.containerMenu).perform();
        String fokusFarbeStartSeite=wibuTestPage.containerMenu.getCssValue("background-color");
        ReusableMethods.waitFor(3);
        System.out.println("Alte Farbe: "+alteFarbeStartSeite);
        System.out.println("Fokus Farbe: "+fokusFarbeStartSeite);
        Assert.assertFalse(alteFarbeStartSeite.equals(fokusFarbeStartSeite));
//a testi
       String alteFarbe=wibuTestPage.zertifikatFarbe.getCssValue("background-color");

        ReusableMethods.waitFor(3);
        actions.moveToElement(wibuTestPage.zertifikatFarbe).perform();
        String fokusFarbe=wibuTestPage.zertifikatFarbe.getCssValue("background-color");
        ReusableMethods.waitFor(3);
        System.out.println("Alte Farbe: "+alteFarbe);
        System.out.println("Fokus Farbe: "+fokusFarbe);
        Assert.assertFalse(alteFarbe.equals(fokusFarbe));


 //b testi
        ReusableMethods.waitFor(5);
        wibuTestPage.zertifikatFarbe.click();
        String geklickteFarbe=wibuTestPage.zertifikatFarbe.getCssValue("background-color");
        System.out.println("Geklickte Farbe: "+geklickteFarbe);
        Assert.assertFalse(fokusFarbe.equals(geklickteFarbe));




Driver.closeDriver();














    }
}

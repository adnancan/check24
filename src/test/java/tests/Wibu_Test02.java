package tests;



import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Wibu_Login_Page;
import pages.Wibu_Test_Page;
import utilities.ConfigReader;

import utilities.Driver;
import utilities.ReusableMethods;


import java.util.ArrayList;
import java.util.List;

public class Wibu_Test02 {
    Wibu_Login_Page loginPage = new Wibu_Login_Page();
    Wibu_Test_Page wibuTestPage = new Wibu_Test_Page();

    @Test
    public void wibuTestCmCloutStarseite() {
        loginPage.userLogin();

        Assert.assertTrue(wibuTestPage.TextStartseite.isDisplayed());
        ReusableMethods.waitForVisibility(wibuTestPage.appAccount, 15);

        Assert.assertTrue(wibuTestPage.iconHerunterladen.isEnabled());

        Assert.assertTrue(wibuTestPage.appSettings.isEnabled());

        Assert.assertTrue(wibuTestPage.appUserHelp.isEnabled());

        Assert.assertTrue(wibuTestPage.appAccount.isEnabled());


        String expectedCodeMeterDashboard=ConfigReader.getProperty("expectedCodeMeterDashboard");
        String actualCodeMeterDashboard=wibuTestPage.codeMeterText.getText();
        Assert.assertEquals(actualCodeMeterDashboard,expectedCodeMeterDashboard);



        ReusableMethods.waitForVisibility(wibuTestPage.appAccount, 15);

        Assert.assertTrue(wibuTestPage.iconHerunterladen.isEnabled());

        Assert.assertTrue(wibuTestPage.appSettings.isEnabled());

        Assert.assertTrue(wibuTestPage.appUserHelp.isEnabled());

        Assert.assertTrue(wibuTestPage.appAccount.isEnabled());

        Assert.assertTrue(wibuTestPage.datenschutz.isEnabled());

        Assert.assertTrue(wibuTestPage.impressum.isEnabled());

        Assert.assertTrue(wibuTestPage.kontakt.isEnabled());
       List<String> expectedList=new ArrayList<>();
       expectedList.add("Startseite");
        expectedList.add("Container");
        expectedList.add("Zertifikate");
        expectedList.add("Upload");
        expectedList.add("Nutzungsberichte");
        for (int i = 0; i < wibuTestPage.hauptMenuIcons.size(); i++) {
           Assert.assertEquals(expectedList.get(i),wibuTestPage.hauptMenuIcons.get(i));

           Driver.closeDriver();
        }





    }
}

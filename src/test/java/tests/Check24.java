package tests;


import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Check24_Page;
import utilities.ConfigReader;
import utilities.Driver;
import utilities.ReusableMethods;
import utilities.TestBaseRapor;


public class Check24 extends TestBaseRapor {

    @Test
    public void check24Test() {
        Check24_Page check24 = new Check24_Page();

        //1. Browse the following link https://www.check24.de/kfz-versicherung/
        Driver.getDriver().get(ConfigReader.getProperty("url_check24"));
        extentTest.info("url ist aktiv");

        //Hier sieht man eine Anzeige, deswegen musste ich sie klicken, damit die andere Schritte ausführen können.
        check24.closeThin.click();

        //Es kommt eine Cookies
        check24.cookies.click();

        //2. Click ‚Tarif vergleichen‘.
        check24.tarifVergeleichen.click();
        extentTest.info("Tarif vergleich wird  geklickt");

        //3. Verischerungswechsel’ option will be selected per default. Click ‘Weiter’ on Situation
        ReusableMethods.waitForVisibility(check24.weiterButton, 10);
        check24.weiterButton.click();
        extentTest.info("weiterButton wurde angeklickt");

        //FotoCheck
        check24.fotoCHECK.click();

        // 4. Click radio button Fahrzeugschein.
        ReusableMethods.waitForVisibility(check24.fahrzeugscheinButton, 20);
        check24.fahrzeugscheinButton.click();
        extentTest.info("Fahrzeugscheinradiobutton wurde angeklickt");

        //5. Enter Herstellernummer as '0588'
        check24.herstelleNummer.sendKeys("0588");
        extentTest.info("Herstellernummer wurde eingegeben");

        //6. Enter Typschlüsselnummer as 'ACI'
        check24.typSchlüsselNummer.sendKeys("ACI");
        extentTest.info("Typschlüsselnummer wurde eingegeben");

        ReusableMethods.waitForVisibility(check24.textBox, 10);
        //7. Verify 'Erstzulassung des Fahrzeugs' text box is displayed
        Assert.assertTrue(check24.textBox.isDisplayed());
        extentTest.info("Textbox der Erstzulassung des Fahrzeugs wurde angezeigt");

        //8. Click button 'Weiter'.
        check24.weiterButton.click();
        extentTest.info("weiterButton wurde angeklickt");

        //9. Verify Error message 'Bitte geben Sie das Datum der Erstzulassung des KFZ an.?' Is displayed
        //Dies sollte die Nachricht (Bitte geben Sie das Datum der Erstzulassung des KFZ an.) sein, damit der Test vollständig bestanden wird.
        String actualErstzulassungText = check24.erstzulassungTextMessage.getText();
        System.out.println(actualErstzulassungText);
        String expectedErstzulassungText = ConfigReader.getProperty("expectedErstzulassungText1");
      Assert.assertEquals(actualErstzulassungText, expectedErstzulassungText);
        extentTest.info("Errormessage wurde verifiziert");

        //10. Enter two years older date in ‘Erstzulasssung des Fahrzeugs’ and ‚Versicherungsbeginn‘ as todays date. Page will look like this:
        check24.textBox.sendKeys("28112019");
        extentTest.info("Erstzulasssungsdatum wurde verifiziert");
        check24.versicherungBeginDatum.sendKeys("01.12.2021");
        extentTest.info("Versicherungsbeginnsdatum wurde eingegeben");

        //11. Click 'Weiter'.
        check24.weiterButton.click();
        extentTest.info("weiterButton wurde angeklickt");

        //12. Verify that input values are accepted on clicking ‘weiter’ and 'Nutzung des Fahrzeug' section is displayed
        Assert.assertTrue(check24.nutzungFahrzeugTitle.isDisplayed());
        extentTest.info("Nutzung des Fahrzeug wurde verifiziert");

        //Stop the test case
        Driver.closeDriver();
        extentTest.info("Testcase wurde gestoppt");



    }


}


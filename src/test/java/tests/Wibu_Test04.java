package tests;

import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Wibu_Login_Page;
import pages.Wibu_Test_Page;
import utilities.Driver;
import utilities.ReusableMethods;

public class Wibu_Test04 {

    Wibu_Login_Page loginPage = new Wibu_Login_Page();
    Wibu_Test_Page wibuTestPage = new Wibu_Test_Page();

    @Test
    public void containerSelect(){
        loginPage.userLogin();
       String alteFarbe=wibuTestPage.containerAnzeigen.getCssValue("background-color");
        Actions actions=new Actions(Driver.getDriver());
        actions.moveToElement(wibuTestPage.containerAnzeigen).perform();
        String fokusFarbe=wibuTestPage.containerAnzeigen.getCssValue("background-color");
        System.out.println("Alte Farbe: "+alteFarbe);
        System.out.println("Fokus Farbe: "+fokusFarbe);
        Assert.assertFalse(alteFarbe.equals(fokusFarbe));


        ReusableMethods.waitFor(3);
        wibuTestPage.startseiteIcon.click();
        Assert.assertFalse(wibuTestPage.zuruckSymbol.isEnabled());
        ReusableMethods.waitFor(3);
        wibuTestPage.containerAnzeigen.click();
        Assert.assertTrue(wibuTestPage.containerText.isDisplayed());
        ReusableMethods.waitFor(3);
        Assert.assertTrue(wibuTestPage.zuruckSymbol.isEnabled());
        wibuTestPage.zuruckSymbol.click();

        ReusableMethods.waitFor(3);
        wibuTestPage.updatedateienEinspielen.click();
        Assert.assertTrue(wibuTestPage.updateDateienText.isDisplayed());
        Assert.assertTrue(wibuTestPage.zuruckSymbol.isEnabled());
        wibuTestPage.zuruckSymbol.click();
        Assert.assertTrue(wibuTestPage.uploadTitel.isDisplayed());
        Assert.assertTrue(wibuTestPage.zuruckSymbol.isEnabled());
        ReusableMethods.waitFor(2);
        wibuTestPage.zuruckSymbol.click();
        Assert.assertFalse(wibuTestPage.zuruckSymbol.isEnabled());

 Driver.closeDriver();

    }
}

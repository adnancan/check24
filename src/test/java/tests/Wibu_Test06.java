package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Wibu_Login_Page;
import pages.Wibu_Test_Page;
import utilities.Driver;
import utilities.ReusableMethods;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

public class Wibu_Test06 {

    Wibu_Login_Page loginPage = new Wibu_Login_Page();
    Wibu_Test_Page wibuTestPage = new Wibu_Test_Page();

    @Test
    public void contentUploadTest() throws AWTException {
        loginPage.userLogin();
       // wibuTestPage.iconUpload.click();



       WebElement selectFilesBtn = Driver.getDriver().findElement(By.className("buttonlike"));
       // String filepath = "C:\\Users\\admin\\Desktop\\FLOWER.jpg";

// Click on Select Files option on the webpage
        selectFilesBtn.click();
       // wibuTestPage.dateienAuswählen.click();
        ReusableMethods.waitFor(3);

// creating object of Robot class
        Robot rb = new Robot();

// copying File path to Clipboard  // Dateipfad in die Zwischenablage kopieren ---// dosya yolunu panoya kopyalama
        StringSelection str = new StringSelection("C:\\Users\\admin\\Desktop\\FLOWER.jpg");
        //C:\Users\admin\Desktop
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);

// press Control+V for pasting
        rb.keyPress(KeyEvent.VK_CONTROL);
        rb.keyPress(KeyEvent.VK_V);

// release Control+V for pasting   Objekt der Roboterklasse erstellen
         rb.keyRelease(KeyEvent.VK_CONTROL);
        rb.keyRelease(KeyEvent.VK_V);

// for pressing and releasing Enter--zum Drücken und Loslassen von---Enter'a basıp bırakmak için
        rb.keyPress(KeyEvent.VK_ENTER);
        rb.keyRelease(KeyEvent.VK_ENTER);
        System.out.println(wibuTestPage.errorText.getText());
        Assert.assertTrue(wibuTestPage.errorText.isEnabled());

        Driver.closeDriver();
/*
        wibuTestPage.iconUpload.click();


        String alteFarbeStartSeite=wibuTestPage.zertifikatFarbe.getCssValue("background-color");
        Actions actions=new Actions(Driver.getDriver());
        ReusableMethods.waitFor(3);
        actions.moveToElement(wibuTestPage.containerMenü).perform();
        String fokusFarbeStartSeite=wibuTestPage.containerMenü.getCssValue("background-color");
        ReusableMethods.waitFor(3);

        //wibuTestPage.dateienAuswählen.click();
        String datePath = System.getProperty("user.home") + "\\Desktop\\FLOWER.jpg";
       // String datePath="C:\\Users\\admin\\Desktop\\140-.WibuCmRaU";
        System.out.println(datePath);

       ReusableMethods.waitFor(5);
        wibuTestPage.dateienAuswählen.sendKeys(datePath);*/





    }


}

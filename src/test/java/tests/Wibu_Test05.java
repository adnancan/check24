package tests;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.Wibu_Login_Page;
import pages.Wibu_Test_Page;
import utilities.ConfigReader;
import utilities.Driver;
import utilities.ReusableMethods;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Wibu_Test05 {

    Wibu_Login_Page loginPage = new Wibu_Login_Page();
    Wibu_Test_Page wibuTestPage = new Wibu_Test_Page();

    @Test
    public void wibuTest2Content() {
        loginPage.userLogin();

        //2.2 Content->CmContainer erstellen –  (a) einfacher Durchlauf

        ReusableMethods.waitFor(3);
        wibuTestPage.containerErstellenButton.click();

        //wibuTestPage.anzahlContainer.sendKeys(ConfigReader.getProperty("anzahl"));
        ReusableMethods.waitFor(3);
        wibuTestPage.containerName.sendKeys(ConfigReader.getProperty("name1"));

        wibuTestPage.drobdownCmActID.click();
        ReusableMethods.waitFor(2);
        wibuTestPage.drobdownAuswuhlen.click();
        ReusableMethods.waitFor(2);
        wibuTestPage.weiterButton.click();
        ReusableMethods.waitFor(2);
        wibuTestPage.weiterButton.click();
        ReusableMethods.waitFor(2);
        wibuTestPage.kleinUploadSymbol.click();
        ReusableMethods.waitFor(3);
        Assert.assertTrue(wibuTestPage.orangeFarbeSchlusselSymbol.isDisplayed());
        String orangeFarbe = wibuTestPage.orangeFarbeSchlusselSymbol.getCssValue("col-warning");


        System.out.println("orange Farbe " + orangeFarbe);
        wibuTestPage.anmeldeDaten.click();
        ReusableMethods.waitFor(4);

        wibuTestPage.kleinUploadSymbol.click();
        String heruntergeladenFarbe = wibuTestPage.kleinUploadSymbol.getCssValue("background-color");
        System.out.println("heruntergeladen Farbe " + heruntergeladenFarbe);
        ReusableMethods.waitFor(5);
        Assert.assertFalse(orangeFarbe.equals(heruntergeladenFarbe));
        wibuTestPage.zuruckUbersicht.click();
        wibuTestPage.suchbegriff.sendKeys(ConfigReader.getProperty("name1") + Keys.ENTER);

        ReusableMethods.waitFor(5);
        String tabllaText = wibuTestPage.gesehenUbersicht.getText();
        ReusableMethods.waitFor(3);
        String expectedTestName = ConfigReader.getProperty("name1");
        System.out.println("=========================================");
        System.out.println("Angesehenen Text =" + tabllaText.toString());
        Assert.assertEquals(tabllaText, expectedTestName);


    }
}